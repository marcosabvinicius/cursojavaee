package br.com.estudosjavaee.servlets;

import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.estudosjavaee.infra.FileSaver;

@WebServlet("/file/*")
public class FileServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
       
    	/*
    	 * Dentro do método service(), conseguimos pegar o que vier pelo * por meio do objeto req.getRequestURI(). 
    	 * O método nos retorna toda a URL, mas só nos interessa o que vier depois de /file, então faremos um split("/file") e 
    	 * do Split teremos dois lados, valor [0] será o que vem antes do /file e [1] o que vem depois. Queremos este último, 
    	 * uma vez que é isso que temos salvo no banco de dados. Desta forma, já teremos o path.    	 */	
    	String path = req.getRequestURI().split("/file")[1];
    	
    	/*
    	 * Podemos juntar o path que temos com o caminho fixo do nosso FileSaver para conseguirmos o caminho completo do arquivo.
    	 *  Passaremos as duas informações para o Paths e assim obtemos um Path como sendo a fonte do nosso arquivo.    	 */
    	Path source = Paths.get(FileSaver.SERVER_PATH + "/" + path);
    	
    	/*
    	 * O Path servirá como fonte para o FileNameMap conseguir chegar no arquivo e obter o contentType. O FileNameMap pode ser
    	 *  obtido usando a classe URLConnection chamando o método estático da classe getFileNameMap.*/
    	FileNameMap fileNameMap = URLConnection.getFileNameMap();
    	
    	/*
    	 * Pelo fileNameMap, chamaremos o método getContentTypeFor() passando nosso source. Só temos antes que informar que o protocolo 
    	 * de acesso é file: para que o FileNameMap possa pegar o arquivo corretamente. Assim sendo, temos:*/
    	String contentType = fileNameMap.getContentTypeFor("file:"+source);
    	
    	res.reset(); //limpar o response para evitar quaisquer dados inválidos
    	res.setContentType(contentType);
    	res.setHeader("Content-Length", String.valueOf(Files.size(source)));
    	res.setHeader("Content-Disposition", "filename=\""+source.getFileName().toString() + "\"");
    	
    	FileSaver.transfer(source, res.getOutputStream());

    }

    
    
    
    
    
}