package br.com.estudosjavaee.beans;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import javax.transaction.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import br.com.estudosjavaee.conf.FacesContextProducer;
import br.com.estudosjavaee.dao.AutorDao;
import br.com.estudosjavaee.dao.LivroDao;
import br.com.estudosjavaee.infra.FileSaver;
import br.com.estudosjavaee.modelo.Autor;
import br.com.estudosjavaee.modelo.Livro;

@Named
@RequestScoped
public class AdminLivrosBean {
	
	private Livro livro = new Livro();
	
	@Inject
	private LivroDao livroDao;
	
	@Inject
	private AutorDao autorDao;
	
	@Inject
	private FacesContext context;
		
	private Part capaLivro;
	
	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}
		
	public LivroDao getLivroDao() {
		return livroDao;
	}

	public void setLivroDao(LivroDao livroDao) {
		this.livroDao = livroDao;
	}

	public AutorDao getAutorDao() {
		return autorDao;
	}

	public void setAutorDao(AutorDao autorDao) {
		this.autorDao = autorDao;
	}
	
	public Part getCapaLivro() {
		return capaLivro;
	}

	public void setCapaLivro(Part capaLivro) {
		this.capaLivro = capaLivro;
	}

	@Transactional
	public String salvar() throws IOException {

		System.out.println("Livro:" + livro);
		livroDao.salvar(livro);
		FileSaver fileSaver = new FileSaver(); // Nossa nova classe
		livro.setCapaPath(fileSaver.write(capaLivro, "livros")); // Já chamamos o método write e já retornamos o path direto para o Livro		
		
		context = FacesContext.getCurrentInstance();
		context.getExternalContext().getFlash().setKeepMessages(true); //Ativação do Flash Scope
		context.addMessage(null, new FacesMessage("Livro Cadastrado !"));
		
		return "/livros/lista?faces-redirect=true";

	}//salvar
	
	public List<Autor> getAutores(){
		return autorDao.listar();
	}//getAutores
	
	
	
}//class
