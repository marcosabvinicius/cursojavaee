package br.com.estudosjavaee.beans;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import br.com.estudosjavaee.dao.LivroDao;
import br.com.estudosjavaee.modelo.Livro;

@Model
public class LivroDetalheBean {
	
	@Inject
	private LivroDao livroDao;
	
	private Integer id;
	
	private Livro livro;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public void carregarDetalhe() {
		this.livro = livroDao.buscarPorId(id);
	}

}
