package br.com.estudosjavaee.beans;

import java.util.List;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import br.com.estudosjavaee.dao.LivroDao;
import br.com.estudosjavaee.modelo.Livro;

@Model
public class HomeBean {
	
	@Inject
	private LivroDao livroDao;
	
	public List<Livro>ultimosLancamentos(){
		return livroDao.ultimosLancamentos();
	}
	
	public List<Livro>demaisLivros(){
		return livroDao.demaisLivros();
	}
	
}
