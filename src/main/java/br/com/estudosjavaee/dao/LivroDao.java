package br.com.estudosjavaee.dao;

import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;

import br.com.estudosjavaee.modelo.Livro;

@Stateful
public class LivroDao {
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager manager;
	
	@Transactional
	public void salvar(Livro livro) {
		//manager.getTransaction().begin();
		manager.persist(livro);
		//manager.getTransaction().commit();
	}//salvar
	
	
	public List<Livro> listar(){
		
		String jpql = "select l from Livro l join fetch l.autores";
		
		return manager.createQuery(jpql, Livro.class).getResultList();
		
	}//listar


	public List<Livro> ultimosLancamentos() {
		String jpql = "select l from Livro l order by l.id desc";
		return manager.createQuery(jpql, Livro.class).setMaxResults(5).getResultList();
	}


	public List<Livro> demaisLivros() {
		String jpql = "select l from Livro l order by l.id desc";
		return manager.createQuery(jpql, Livro.class).setFirstResult(5).getResultList();
	}


	public Livro buscarPorId(Integer id) {
		
		return manager.find(Livro.class, id);
		//Antes da annotetion 2Satteful era assim:
		//String jpql = "select l from Livro l join fetch l.autores where l.id = :id";
		//return manager.createQuery(jpql, Livro.class).setParameter("id",id).getSingleResult();
		
	}
	
}//class