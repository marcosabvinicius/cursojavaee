package br.com.estudosjavaee.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.estudosjavaee.modelo.Autor;

@FacesConverter("autorConverter")
public class AutorConverter implements Converter{
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String id) {
		
		//primeiro há a verificação se a string recebida está vazia
		if(id == null || id.trim().isEmpty()) {
			return null;
		}
		
		//realizar uma instância de autor, para pegar o método setId
		Autor autor = new Autor();
		//verificar o valor inteiro da string recebida
		autor.setId(Integer.valueOf(id));
		
		return autor;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object autorObject) {
		
		if(autorObject == null) {
			return null;
		}//if
		
		Autor autor = (Autor) autorObject;
		return autor.getId().toString();
		
	}
}
